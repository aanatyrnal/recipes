package router

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/aanatyrnal/recipes/handler"
)

// SetupRoutes func
func SetupRoutes(app *fiber.App) {
	// grouping
	api := app.Group("/api")
	v1 := api.Group("/recipe")
	v2 := api.Group("/ingredient")
	v3 := api.Group("/user")
	// routes
	v1.Post("/", handler.CreateRecipe)

	v2.Post("/", handler.CreateIngredient)
	v1.Post("/ingredient", handler.CreateRecipeIngredient)
	v1.Post("/step", handler.CreateCookingStep)

	v1.Get("/", handler.GetAllRecipes)
	v1.Get("/sort", handler.GetAllRecipesSort)
	v1.Get("/ingredient", handler.GetAllRecipesIngredients)
	v1.Get("/:id", handler.GetRecipe)
	v1.Put("/:id", handler.UpdateRecipe)
	v1.Delete("/:id", handler.DeleteRecipe)

	v3.Post("/", handler.CreateUser)
	v3.Post("/login", handler.Login)
	v3.Get("/", handler.GetAllUsers)
	v3.Get("/:id", handler.GetUser)
	v3.Put("/:id", handler.UpdateUser)
	v3.Delete("/:id", handler.DeleteUserByID)
}
