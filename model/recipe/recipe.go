package recipe

import "gorm.io/gorm"

type Recipe struct {
	gorm.Model
	Name        string `json:"name"`
	Description string `json:"description"`
}

type Ingredient struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	//CountIngredient float64 `json:"count_ingredient"`
}

type RecipeIngredient struct {
	ID              int     `json:"id"`
	RecipeID        uint    `json:"recipe_id"`
	IngredientID    int     `json:"ingredient_id"`
	CountIngredient float64 `json:"count_ingredient"`
}

type CookingStep struct {
	//gorm.Model
	ID          int    `json:"id"`
	RecipeID    uint   `json:"recipe_id"`
	IDStep      int    `json:"id_step"`
	Description string `json:"description"`
	TimeStep    int    `json:"time_step"`
	//TimeStep    time.Time `json:"time_step"`
}
