package handler

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/aanatyrnal/recipes/db"
	"gitlab.com/aanatyrnal/recipes/model/recipe"
	"gitlab.com/aanatyrnal/recipes/model/recipe_filter"
	"gitlab.com/aanatyrnal/recipes/model/recipe_read"
	"log"
	"net/url"
	"os"
	"strings"
)

func CreateRecipe(c *fiber.Ctx) error {
	JWT_KEY := os.Getenv("JWT_SECRET_KEY")

	tokenString := string(c.Request().Header.Peek("Authorization"))
	tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

	if tokenString == "" {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(JWT_KEY), nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	username := claims["sub"]
	fmt.Println("username", username)

	db := db.DB.Db

	recipe := new(recipe.Recipe)
	err = c.BodyParser(recipe)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Проблемы с входными данными", "data": err})
	}

	err = db.Create(recipe).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Не удалось создать рецепт", "data": err})
	}

	return c.Status(201).JSON(fiber.Map{"status": "success", "message": "Рецепт создан", "data": recipe})
}

func CreateIngredient(c *fiber.Ctx) error {
	JWT_KEY := os.Getenv("JWT_SECRET_KEY")

	tokenString := string(c.Request().Header.Peek("Authorization"))
	tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

	if tokenString == "" {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(JWT_KEY), nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	username := claims["sub"]
	fmt.Println("username", username)

	db := db.DB.Db

	ingredient := new(recipe.Ingredient)
	err = c.BodyParser(ingredient)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Something's wrong with your input", "data": err})
	}

	err = db.Create(ingredient).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create ingredient", "data": err})
	}

	return c.Status(201).JSON(fiber.Map{"status": "success", "message": "Ingredient has been created", "data": ingredient})
}

func CreateRecipeIngredient(c *fiber.Ctx) error {
	JWT_KEY := os.Getenv("JWT_SECRET_KEY")

	tokenString := string(c.Request().Header.Peek("Authorization"))
	tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

	if tokenString == "" {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(JWT_KEY), nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	username := claims["sub"]
	fmt.Println("username", username)

	db := db.DB.Db

	recipeIngredient := new(recipe.RecipeIngredient)
	err = c.BodyParser(recipeIngredient)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Something's wrong with your input", "data": err})
	}

	err = db.Create(recipeIngredient).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create recipeIngredient", "data": err})
	}

	return c.Status(201).JSON(fiber.Map{"status": "success", "message": "RecipeIngredient has been created", "data": recipeIngredient})
}

func CreateCookingStep(c *fiber.Ctx) error {
	JWT_KEY := os.Getenv("JWT_SECRET_KEY")

	tokenString := string(c.Request().Header.Peek("Authorization"))
	tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

	if tokenString == "" {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(JWT_KEY), nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	username := claims["sub"]
	fmt.Println("username", username)

	db := db.DB.Db

	cookingStep := new(recipe.CookingStep)
	err = c.BodyParser(cookingStep)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Something's wrong with your input", "data": err})
	}

	err = db.Create(cookingStep).Error
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create recipe", "data": err})
	}

	return c.Status(201).JSON(fiber.Map{"status": "success", "message": "Recipe has been created", "data": cookingStep})
}

//func CreateRecipe(c *fiber.Ctx) error {
//	db := db.DB.Db
//
//	recipe := new(model.Recipe)
//	err := c.BodyParser(recipe)
//	if err != nil {
//		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Something's wrong with your input", "data": err})
//	}
//
//	err = db.Create(recipe).Error
//	if err != nil {
//		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create recipe", "data": err})
//	}
//
//	for _, ingredient := range recipe.Ingredients {
//		existingIngredient := new(model.Ingredient)
//		if err := db.Model(existingIngredient).Where("name = ?", ingredient.Name).First(existingIngredient).Error; err != nil {
//			if errors.Is(err, gorm.ErrRecordNotFound) {
//				// Ингредиент не найден, создаем новый и сохраняем его
//				newIngredient := model.Ingredient{
//					Name:            ingredient.Name,
//					CountIngredient: ingredient.CountIngredient,
//				}
//				if err := db.Create(&newIngredient).Error; err != nil {
//					return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create ingredient", "data": err})
//				}
//				existingIngredient.ID = ingredient.ID // Присвоить ID ингредиента
//			} else {
//				return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error retrieving ingredient", "data": err})
//			}
//		} else {
//			existingIngredient.CountIngredient = ingredient.CountIngredient
//			if err := db.Save(existingIngredient).Error; err != nil {
//				return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not update ingredient", "data": err})
//			}
//		}
//
//		existingIngredient.ID = ingredient.ID
//
//		existingRecipeIngredient := new(model.RecipeIngredient)
//		if err := db.Model(existingRecipeIngredient).
//			Where("recipe_id = ? AND ingredient_id = ?", recipe.ID, existingIngredient.ID).
//			First(existingRecipeIngredient).Error; err != nil {
//			if errors.Is(err, gorm.ErrRecordNotFound) {
//				// Связь не найдена, создаем новую запись
//				recipeIngredient := model.RecipeIngredient{
//					RecipeID:        recipe.ID,
//					IngredientID:    existingIngredient.ID,
//					CountIngredient: ingredient.CountIngredient,
//				}
//				if err := db.Create(&recipeIngredient).Error; err != nil {
//					return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create recipe ingredient", "data": err})
//				}
//			} else {
//				return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Error retrieving recipe ingredient", "data": err})
//			}
//		}
//	}
//
//	for _, step := range recipe.CookingSteps {
//		// Устанавливаем RecipeID для связи с рецептом
//		step.RecipeID = recipe.ID
//		// Создание новой записи в таблице cooking_steps
//		newStep := model.CookingStep{
//			RecipeID:    step.RecipeID,
//			IDStep:      step.IDStep,
//			Description: step.Description,
//			TimeStep:    step.TimeStep,
//		}
//		if err := db.Create(&newStep).Error; err != nil {
//			return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Could not create cooking step", "data": err})
//		}
//	}
//
//	return c.Status(201).JSON(fiber.Map{"status": "success", "message": "Recipe has been created", "data": recipe})
//}

func GetAllRecipesSort(c *fiber.Ctx) error {
	db := db.DB.Db

	filter := c.Query("recipe_filter")
	//sort := c.Query("sort")

	// Define the base query
	baseQuery := db.Model(&recipe_read.Recipe{}).
		Select("recipes.*, SUM(cooking_steps.time_step) AS total_cooking_time, STRING_AGG(DISTINCT ingredients.name, ', ') AS ingredient_list, STRING_AGG(DISTINCT CONCAT(CAST(cooking_steps.id_step AS text), ': ', cooking_steps.description), ', ') AS cooking_steps_list").
		Joins("JOIN cooking_steps ON cooking_steps.recipe_id = recipes.id").
		Joins("JOIN recipe_ingredients ON recipes.id = recipe_ingredients.recipe_id").
		Joins("JOIN ingredients ON ingredients.id = recipe_ingredients.ingredient_id").
		Where("recipes.deleted_at IS NULL").
		Group("recipes.id").
		Order("total_cooking_time ASC")

	if filter != "" {
		baseQuery = baseQuery.Where("recipes.name LIKE ?", "%"+filter+"%")
	}

	var recipes []recipe_filter.Recipe
	if err := baseQuery.Find(&recipes).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Failed to retrieve recipes", "data": nil})
	}

	if len(recipes) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Recipes not found", "data": nil})
	}

	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Recipes Found", "data": recipes})
}

func GetAllRecipes(c *fiber.Ctx) error {
	db := db.DB.Db
	var recipes []recipe_read.Recipe
	db.Preload("Ingredients").Preload("CookingSteps").Find(&recipes)
	if len(recipes) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Recipes not found", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Recipes Found", "data": recipes})
}

func GetAllRecipesIngredients(c *fiber.Ctx) error {
	db := db.DB.Db
	ingredients := c.Query("ingredients")

	decodedIngredients, err := url.QueryUnescape(ingredients)
	if err != nil {
		log.Fatal(err)
	}

	ingredientSlice := strings.Split(decodedIngredients, ",")

	var recipes []recipe_read.Recipe
	if err := db.Preload("Ingredients").
		Joins("JOIN recipe_ingredients ON recipes.id = recipe_ingredients.recipe_id").
		Joins("JOIN ingredients ON recipe_ingredients.ingredient_id = ingredients.id").
		Where("ingredients.name IN (?)", ingredientSlice).
		Group("recipes.id").
		Having("COUNT(DISTINCT ingredients.name) = ?", len(ingredientSlice)).
		Find(&recipes).Error; err != nil {
		log.Fatal(err)
	}

	return c.JSON(fiber.Map{
		"status":  "success",
		"message": "Recipes Found",
		"data":    recipes,
	})
}

func GetRecipe(c *fiber.Ctx) error {
	db := db.DB.Db
	id := c.Params("id")
	var recipe recipe_read.Recipe
	db.Preload("Ingredients").Preload("CookingSteps").Find(&recipe, "id = ?", id)
	if recipe.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Recipe not found", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Recipe Found", "data": recipe})
}

func UpdateRecipe(c *fiber.Ctx) error {
	JWT_KEY := os.Getenv("JWT_SECRET_KEY")

	tokenString := string(c.Request().Header.Peek("Authorization"))
	tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

	if tokenString == "" {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(JWT_KEY), nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	username := claims["sub"]
	fmt.Println("username", username)

	type updateRecipe struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		//Ingredients  []Ingredient  `json:"ingredients" gorm:"many2many:recipe_ingredients;"`
		//CookingSteps []CookingStep `json:"cooking_steps" gorm:"foreignkey:RecipeID;"`
	}
	db := db.DB.Db
	var recipe recipe.Recipe
	id := c.Params("id")
	db.Find(&recipe, "id = ?", id)
	if recipe.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "recipe not found", "data": nil})
	}
	var updateRecipeData updateRecipe
	err = c.BodyParser(&updateRecipeData)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Something's wrong with your input", "data": err})
	}
	recipe.Name = updateRecipeData.Name
	recipe.Description = updateRecipeData.Description
	db.Save(&recipe)
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "recipe Found", "data": recipe})
}

func DeleteRecipe(c *fiber.Ctx) error {
	JWT_KEY := os.Getenv("JWT_SECRET_KEY")

	tokenString := string(c.Request().Header.Peek("Authorization"))
	tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

	if tokenString == "" {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(JWT_KEY), nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Аутентификация не пройдена"})
	}

	username := claims["sub"]
	fmt.Println("username", username)

	db := db.DB.Db
	var recipe recipe.Recipe
	id := c.Params("id")
	db.Find(&recipe, "id = ?", id)
	if recipe.ID == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "recipe not found", "data": nil})
	}
	err = db.Delete(&recipe, "id = ?", id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete recipe", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "recipe deleted"})
}
