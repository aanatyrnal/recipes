package recipe_filter

import "gorm.io/gorm"

type Recipe struct {
	gorm.Model
	//ID           int           `json:"id"`
	Name             string `json:"name"`
	Description      string `json:"description"`
	IngredientList   string `json:"ingredient_list"`
	CookingStepsList string `json:"cooking_steps_list"`
	TotalCookingTime int    `json:"total_cooking_time"`
}
