package handler

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/aanatyrnal/recipes/db"
	"gitlab.com/aanatyrnal/recipes/model"
	"golang.org/x/crypto/bcrypt"
	"os"
	"time"
)

func CreateUser(c *fiber.Ctx) error {
	db := db.DB.Db
	user := new(model.User)

	if err := c.BodyParser(user); err != nil {
		return c.Status(400).JSON(fiber.Map{"status": "error", "message": "Неверный формат запроса"})
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.PasswordHash), bcrypt.DefaultCost)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Не удалось захэшировать пароль"})
	}
	user.PasswordHash = string(hashedPassword)

	user.ID = uuid.New()

	if err := db.Create(&user).Error; err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Не удалось создать пользователя", "data": err})
	}

	user.PasswordHash = ""

	return c.Status(201).JSON(fiber.Map{"status": "success", "message": "Пользователь создан", "data": user})
}

func Login(c *fiber.Ctx) error {
	db := db.DB.Db

	email := c.FormValue("email")
	password := c.FormValue("password")

	var user model.User
	if err := db.Where("email = ?", email).First(&user).Error; err != nil {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Неверные учетные данные"})
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(password)); err != nil {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Неверные учетные данные"})
	}

	// Получаем секретный ключ из .env файла
	secretKey := os.Getenv("JWT_SECRET_KEY")
	if secretKey == "" {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Не удалось получить секретный ключ"})
	}

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["user_id"] = user.ID
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix() // Установка срока действия токена (24 часа)

	tokenString, err := token.SignedString([]byte(secretKey))
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Не удалось создать access token"})
	}

	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "Успешный вход", "data": tokenString})
}

func AuthGetUser(c *fiber.Ctx) error {
	db := db.DB.Db

	token := c.Get("Authorization")

	claims := &model.Claims{}
	tkn, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		secretKey := os.Getenv("JWT_SECRET_KEY")
		return []byte(secretKey), nil
	})
	if err != nil || !tkn.Valid {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Недействительный или истекший токен"})
	}

	var user model.User
	if err := db.Where("id = ?", claims.UserID).First(&user).Error; err != nil {
		return c.Status(401).JSON(fiber.Map{"status": "error", "message": "Пользователь не зарегистрирован"})
	}

	c.Locals("user", user)

	return c.Next()
}

func GetAllUsers(c *fiber.Ctx) error {
	db := db.DB.Db
	var users []model.User
	db.Find(&users)
	if len(users) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Users not found", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "sucess", "message": "Users Found", "data": users})
}

func GetUser(c *fiber.Ctx) error {
	db := db.DB.Db
	// get id params
	id := c.Params("id")
	var user model.User
	// find single user in the database by id
	db.Find(&user, "id = ?", id)
	if user.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "User not found", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "User Found", "data": user})
}

func UpdateUser(c *fiber.Ctx) error {
	type updateUser struct {
		Username string `json:"username"`
	}
	db := db.DB.Db
	var user model.User
	id := c.Params("id")
	db.Find(&user, "id = ?", id)
	if user.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "User not found", "data": nil})
	}
	var updateUserData updateUser
	err := c.BodyParser(&updateUserData)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Something's wrong with your input", "data": err})
	}
	user.Username = updateUserData.Username
	db.Save(&user)
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "users Found", "data": user})
}

func DeleteUserByID(c *fiber.Ctx) error {
	db := db.DB.Db
	var user model.User
	// get id params
	id := c.Params("id")
	// find single user in the database by id
	db.Find(&user, "id = ?", id)
	if user.ID == uuid.Nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "User not found", "data": nil})
	}
	err := db.Delete(&user, "id = ?", id).Error
	if err != nil {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "Failed to delete user", "data": nil})
	}
	return c.Status(200).JSON(fiber.Map{"status": "success", "message": "User deleted"})
}
