package recipe_read

import "gorm.io/gorm"

type Recipe struct {
	gorm.Model
	//ID           int           `json:"id"`
	Name         string        `json:"name"`
	Description  string        `json:"description"`
	Ingredients  []Ingredient  `json:"ingredients" gorm:"many2many:recipe_ingredients;"`
	CookingSteps []CookingStep `json:"cooking_steps" gorm:"foreignkey:RecipeID;"`
	//TotalCookingTime int           `json:"total_cooking_time"`
}

type Ingredient struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	//CountIngredient float64 `json:"count_ingredient"`
}

type CookingStep struct {
	//gorm.Model
	ID          int    `json:"id"`
	RecipeID    uint   `json:"recipe_id"`
	IDStep      int    `json:"id_step"`
	Description string `json:"description"`
	TimeStep    int    `json:"time_step"`
	//TimeStep    time.Time `json:"time_step"`
}
