package model

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type User struct {
	gorm.Model
	ID           uuid.UUID `gorm:"type:uuid;"`
	Username     string    `json:"username"`
	Email        string    `json:"email"`
	PasswordHash string    `json:"password"`
	DateRegister time.Time
	Role         string
}

// Users struct
type Users struct {
	Users []User `json:"users"`
}

type Claims struct {
	UserID uint `json:"user_id"`
	jwt.StandardClaims
}

//func (user *User) BeforeCreate(tx *gorm.DB) (err error) {
//	// UUID version 4
//	user.ID = uuid.New()
//	return
//}
